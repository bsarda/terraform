# ------------------- fixed vars --------------------
# ------- retrieved from the terraform.tfvars -------
# fixed variables for underlying hardware infra
variable "vcserver" {}
variable "vcuser" {}
variable "vcpassword" {}
variable "datacenter" {}
variable "cluster" {}
variable "datastore" {}
# fixed variables for underlying soft infra
variable "dnsservers-dc1" {}
variable "dnsservers-dc2" {}
variable "domain" {}
variable "dnssuffix" {}
variable "dc1-srv2-mask" {}
variable "dc1-srv2-gw" {}
variable "dc2-srv2-mask" {}
variable "dc2-srv2-gw" {}
variable "timezone" {}
variable "template-esx-dc1" {}
variable "template-esx-dc2" {}
variable "esx-vcpu" {}
variable "esx-mem" {}

# ----------------- project vars --------------------
# project variables
# Please indicate the 2-letter hexadecimal project
variable "projectInitials" { default = "AA" }
# Please indicate the full project name
variable "projectName" { default = "Automation Dual site" }
# Please enter the count of ESXi's for each Region and DC.
variable "esxi-emea-dc1-count" {default = 3 }
variable "esxi-emea-dc2-count" {default = 4 }
variable "esxi-emea-dc3-count" {default = 0 }
variable "esxi-amer-dc1-count" {default = 0 }
variable "esxi-amer-dc2-count" {default = 0 }
variable "esxi-amer-dc3-count" {default = 0 }
variable "esxi-apj-dc1-count"  {default = 0 }
variable "esxi-apj-dc2-count"  {default = 0 }
variable "esxi-apj-dc3-count"  {default = 0 }

# nested ESXi definitions
variable "esx-dc1" {
	default = [
		{
			"name" = "e1aaesx01"
			"ip" = "172.20.12.43"
		},
		{
			"name" = "e1aaesx02"
			"ip" = "172.20.12.44"
		},
		{
			"name" = "e1aaesx03"
			"ip" = "172.20.12.45"
		}
	]
}
# ------------------ model vars ---------------------

# ===================================================
# ------------------- providers ---------------------
# Configure the VMware vSphere Provider
provider "vsphere" {
  user                 = "${var.vcuser}"
  password             = "${var.vcpassword}"
  vsphere_server       = "${var.vcserver}"
  allow_unverified_ssl = true
}
# Configure the DNS Provider
provider "dns" {
  update {
    server     = "172.20.1.32"
  }
}

# ===================================================
# ------------------- part: global ------------------

# Create a folder
resource "vsphere_folder" "projectFolder" {
  datacenter = "${var.datacenter}"
  path       = "${var.projectInitials} - ${var.projectName}"
}

# ------------------- part: ESXi-s ------------------
# Create the DNS A record set for the ESXi-s of EMEA DC1
resource "dns_a_record_set" "esxi" {
  #count     = "${length(var.esx-dc1)}"
  count     = "${var.esxi-emea-dc1-count}"
  zone      = "${var.dnssuffix}."
  name      = "${count.index >9 ? lower( "e1${var.projectInitials}esx${count.index+1}" ) : lower( "e1${var.projectInitials}esx0${count.index+1}" ) }"
  #name      = "${lookup(var.esx-dc1[count.index], "name")}"
  #addresses = ["172.20.12.43"]
  addresses = ["${lookup(var.esx-dc1[count.index], "ip")}"]
}
# resource "dns_ptr_record" "e1aaesx01" {
#  zone = "emea.corp.local."
#  name = "r._dns-sd"
#  ptr  = "emea.corp.local."
#}


## esxi-emea-dc1-count

# Create the ESXi-s of EMEA DC1
resource "vsphere_virtual_machine" "esxi" {
  count        = "${var.esxi-emea-dc1-count}"
  datacenter   = "${var.datacenter}"
  cluster      = "${var.cluster}"
  #name         = "${concat("e1",var.projectInitials,"esx",count.index)}"
  #name         = "${lower( "e1${var.projectInitials}esx${count.index}" )}
  name        = "${lookup(var.esx-dc1[count.index], "name")}"
  folder       = "${vsphere_folder.projectFolder.path}"
  vcpu         = "${var.esx-vcpu}"
  memory       = "${var.esx-mem}"
  domain       = "${var.domain}"
  time_zone    = "${var.timezone}"
  dns_suffixes = ["${split(",", var.dnssuffix)}"]
  dns_servers  = ["${split(",", var.dnsservers-dc1)}"]
  network_interface {
    label              = "SYSTEM-Passthrough-pnic0"
    ipv4_address       = "${lookup(var.esx-dc1[count.index], "ip")}"
    ipv4_prefix_length = "${var.dc1-srv2-mask}"
    ipv4_gateway       = "${var.dc1-srv2-gw}"
  }
  disk {
    datastore          = "${var.datastore}"
    template           = "${var.template-esx-dc1}"
  }
}

# Create the ESXi-s of EMEA DC1
#resource "vsphere_virtual_machine" "esxi" {
#  count        = "${length(var.esx-dc1)}"
#  datacenter   = "${var.datacenter}"
#  cluster      = "${var.cluster}"
#  name         = "${lookup(var.esx-dc1[count.index], "name")}"
#  folder       = "${vsphere_folder.projectFolder.path}"
#  vcpu         = "${var.esx-vcpu}"
#  memory       = "${var.esx-mem}"
#  domain       = "${var.domain}"
#  time_zone    = "${var.timezone}"
#  dns_suffixes = ["${split(",", var.dnssuffix)}"]
#  dns_servers  = ["${split(",", var.dnsservers-dc1)}"]
#  network_interface {
#    label              = "SYSTEM-Passthrough-pnic0"
#    ipv4_address       = "${lookup(var.esx-dc1[count.index], "ip")}"
#    ipv4_prefix_length = "${var.dc1-srv2-mask}"
#    ipv4_gateway       = "${var.dc1-srv2-gw}"
#  }
#  disk {
#    datastore          = "${var.datastore}"
#    template           = "${var.template-esx-dc1}"
#  }
#}

# ===================================================

output "esxi_uuids" {
  value = ["${vsphere_virtual_machine.esxi.*.uuid}"]
}
