# ------------------- fixed vars --------------------
# fixed variables for underlying hardware infra
vcserver="mgt-vc"
vcuser="administrator@vsphere.local"
vcpassword="VMware1!"
datacenter="HomeLab"
cluster="Dell_DCS-6005"
datastore="VMFS_ssd-Sandisk-960G"

# fixed variables for underlying soft infra
dnsservers-dc1="172.20.1.32,172.21.1.32"
dnsservers-dc2="172.21.1.32,172.20.1.32"
domain="emea.corp.local"
dnssuffix="emea.corp.local"
dc1-srv2-mask="24"
dc1-srv2-gw="172.20.12.1"
dc2-srv2-mask="24"
dc2-srv2-gw="172.21.12.1"
timezone="Europe/Paris"

# templates and default config
template-esx-dc1="Templates/t_ESXi-6.5a-mk1_DC1"
template-esx-dc2="Templates/t_ESXi-6.5a-mk1_DC2"
esx-vcpu=2
esx-mem=8192
