# --------- retrieved from the *.auto.tfvars --------
# --------------- do not edit, no need --------------
variable "root_vc_name" {}
variable "root_vc_user" {}
variable "root_vc_pass" {}

variable "root_vc_dc" {}
variable "root_vc_cl" {}
variable "root_vc_ds" {}
variable "root_vc_net" {}
# ###################################################

# ----------------- project vars --------------------

# Please indicate the project
variable "project_id" { default = "lab5" }

# Please indicate the full project name
variable "project_name" { default = "VIO-51 and PKS-13" }

# ---------------- nsx infos commons ----------------
# NSX infos common
variable "nsx_sizing" {default = "small"}
	# passwords (same for Manager, Controllers, Edges)
variable "nsx_password" { default = "VMware1!" }
variable "nsx_cli_password" { default = "VMware1!" }
variable "nsx_audit_password" { default = "VMware1!" }
	# ssh (same for Manager, Controllers, Edges)
variable "nsx_ssh_enabled" { default = "True" }
variable "nsx_ssh_root" { default = "True" }
	# networks
variable "nsx_net_mgmt" { default = "VM Network" }
variable "nsx_net_tep" { default = "VM Network" }
variable "nsx_net_uplink1" { default = "VM Network" }
variable "nsx_net_uplink2" { default = "VM Network" }
	# infra commons (considering same L2 for mgmt of mgr, ctrl, edges)
variable "nsx_mask" { default = "255.255.255.0" }
variable "nsx_gw" { default = "172.16.5.1" }
variable "nsx_dns" { default = "172.16.5.5" }
variable "nsx_domain" { default = "corp.local" }
variable "nsx_ntp" { default = "172.16.5.5" }

# NSX infos per appliance
	# manager- specs
variable "nsx_mgr_vmname" { default = "lab5-nsxmgr" }
variable "nsx_hostname" { default = "lab5-nsxmgr" }
variable "nsx_ip" { default = "172.16.5.22" }
variable "nsx_mgr_ova" { default = "C:\\Users\\Administrator\\Downloads\\nsx-unified-appliance-2.3.1.0.0.11294305.ova" }
	# controllers- , note that name will be suffixed by the index of ips
variable "nsx_ctrl_vmname" { default = "lab5-nsxctl-" }
variable "nsx_ctrl_hostname" { default = "lab5-nsxctl-" }
variable "nsx_ctrl_ova" { default = "C:\\Users\\Administrator\\Downloads\\nsx-controller-2.3.1.0.0.11294300.ova" }
variable "nsx_ctrl_ips" { default = {
		"0" = "172.16.5.23"
		"1" = "172.16.5.24"
		"2" = "172.16.5.25"
	} }
	# edges- specs , note that name will be suffixed by the index of ips
variable "nsx_edge_vmname" { default = "lab5-nsxedg-" }  
variable "nsx_edge_hostname" { default = "lab5-nsxedg-" }
variable "nsx_edge_sizing" {default = "large"}
variable "nsx_edge_ova" { default = "C:\\Users\\Administrator\\Downloads\\nsx-edge-2.3.1.0.0.11294325.ova" }
variable "nsx_edge_ips" {default = {
		"0" = "172.16.5.26"
		"1" = "172.16.5.27"
	} }

# ----------------- infra specific -----------------
variable "root_vc_resourcepool" { default = "PRIV/host/LAB/resourcePool" }  # deploy in the cluster, no resource pool (will auto-complete)