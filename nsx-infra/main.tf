# ===================================================
# know working for NSX-T 2.3.x
# ------------------- providers ---------------------
# Configure the VMware vSphere Provider
provider "vsphere" {
  vsphere_server       = "${var.root_vc_name}"
  user                 = "${var.root_vc_user}"
  password             = "${var.root_vc_pass}"
  allow_unverified_ssl = true
}

# ===================================================
# if-else setting variables
#    pool = "${var.root_vc_resourcepool == "" ? "${var.root_vc_dc}/host/${var.root_vc_cl}/Resources" : "${var.root_vc_resourcepool}" }"
# "PRIV/host/LAB/Resources"
resource "null_resource" "resource_pool" {
  triggers {
    pool = "LAB/Resources"
  }
}

# ---------------------------------------------------
# vsphere folder
# DC object
# name          = "${null_resource.resource_pool.triggers.pool}"
data "vsphere_datacenter" "dc" {
  name = "${var.root_vc_dc}"
}

data "vsphere_datastore" "ds" {
  name          = "${var.root_vc_ds}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_compute_cluster" "cl" {
  name          = "${var.root_vc_cl}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_resource_pool" "pool" {
  name          = "${null_resource.resource_pool.triggers.pool}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_network" "net_mgmt" {
    name          = "${var.nsx_net_mgmt}"
    datacenter_id = "${data.vsphere_datacenter.dc.id}"
}
data "vsphere_network" "net_tep" {
    name          = "${var.nsx_net_tep}"
    datacenter_id = "${data.vsphere_datacenter.dc.id}"
}
data "vsphere_network" "net_uplink1" {
    name          = "${var.nsx_net_uplink1}"
    datacenter_id = "${data.vsphere_datacenter.dc.id}"
}
data "vsphere_network" "net_uplink2" {
    name          = "${var.nsx_net_uplink2}"
    datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

# Create a folder
resource "vsphere_folder" "vc_folder" {
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
  type          = "vm"
  path          = "${var.project_id} - ${var.project_name}"
}

# ---------------------------------------------------
# files for govc import ova

resource "local_file" "nsx_mgr_json" {
  content  = "{\"Deployment\":\"${var.nsx_sizing}\",\"DiskProvisioning\":\"thin\",\"IPAllocationPolicy\":\"fixedPolicy\",\"IPProtocol\":\"IPv4\",\"PropertyMapping\":[{\"Key\":\"nsx_passwd_0\",\"Value\":\"${var.nsx_password}\"},{\"Key\":\"nsx_cli_passwd_0\",\"Value\":\"${var.nsx_cli_password}\"},{\"Key\":\"nsx_cli_audit_passwd_0\",\"Value\":\"${var.nsx_audit_password}\"},{\"Key\":\"nsx_hostname\",\"Value\":\"${var.nsx_hostname}\"},{\"Key\":\"nsx_role\",\"Value\":\"nsx-manager\"},{\"Key\":\"nsx_gateway_0\",\"Value\":\"${var.nsx_gw}\"},{\"Key\":\"nsx_ip_0\",\"Value\":\"${var.nsx_ip}\"},{\"Key\":\"nsx_netmask_0\",\"Value\":\"${var.nsx_mask}\"},{\"Key\":\"nsx_dns1_0\",\"Value\":\"${var.nsx_dns}\"},{\"Key\":\"nsx_domain_0\",\"Value\":\"${var.nsx_domain}\"},{\"Key\":\"nsx_ntp_0\",\"Value\":\"${var.nsx_ntp}\"},{\"Key\":\"nsx_isSSHEnabled\",\"Value\":\"${var.nsx_ssh_enabled}\"},{\"Key\":\"nsx_allowSSHRootLogin\",\"Value\":\"${var.nsx_ssh_root}\"}],\"NetworkMapping\":[{\"Name\":\"Network 1\",\"Network\":\"${var.nsx_net_mgmt}\"}],\"MarkAsTemplate\":false,\"PowerOn\":false,\"InjectOvfEnv\":true,\"WaitForIP\":false,\"Name\":\"${var.nsx_mgr_vmname}\"}"
  filename = "nsx_mgr_options.json"
}

resource "local_file" "nsx_ctrl_json" {
  count    = "${length(var.nsx_ctrl_ips)}"
  content  = "{\"Deployment\":\"${var.nsx_sizing}\",\"DiskProvisioning\":\"thin\",\"IPAllocationPolicy\":\"fixedPolicy\",\"IPProtocol\":\"IPv4\",\"PropertyMapping\":[{\"Key\":\"nsx_passwd_0\",\"Value\":\"${var.nsx_password}\"},{\"Key\":\"nsx_cli_passwd_0\",\"Value\":\"${var.nsx_cli_password}\"},{\"Key\":\"nsx_cli_audit_passwd_0\",\"Value\":\"${var.nsx_audit_password}\"},{\"Key\":\"nsx_hostname\",\"Value\":\"${"${var.nsx_ctrl_hostname}${count.index}"}\"},{\"Key\":\"nsx_gateway_0\",\"Value\":\"${var.nsx_gw}\"},{\"Key\":\"nsx_ip_0\",\"Value\":\"${lookup(var.nsx_ctrl_ips,count.index)}\"},{\"Key\":\"nsx_netmask_0\",\"Value\":\"${var.nsx_mask}\"},{\"Key\":\"nsx_dns1_0\",\"Value\":\"${var.nsx_dns}\"},{\"Key\":\"nsx_domain_0\",\"Value\":\"${var.nsx_domain}\"},{\"Key\":\"nsx_ntp_0\",\"Value\":\"${var.nsx_ntp}\"},{\"Key\":\"nsx_isSSHEnabled\",\"Value\":\"${var.nsx_ssh_enabled}\"},{\"Key\":\"nsx_allowSSHRootLogin\",\"Value\":\"${var.nsx_ssh_root}\"}],\"NetworkMapping\":[{\"Name\":\"Network 1\",\"Network\":\"${var.nsx_net_mgmt}\"}],\"MarkAsTemplate\":false,\"PowerOn\":false,\"InjectOvfEnv\":true,\"WaitForIP\":false,\"Name\":\"${"${var.nsx_ctrl_vmname}${count.index}"}\"}"
  filename = "nsx_ctrl_${count.index}_options.json"
}

resource "local_file" "nsx_edge_json" {
  count    = "${length(var.nsx_edge_ips)}"
  content  = "{\"Deployment\":\"${var.nsx_edge_sizing}\",\"DiskProvisioning\":\"thin\",\"IPAllocationPolicy\":\"fixedPolicy\",\"IPProtocol\":\"IPv4\",\"PropertyMapping\":[{\"Key\":\"nsx_passwd_0\",\"Value\":\"${var.nsx_password}\"},{\"Key\":\"nsx_cli_passwd_0\",\"Value\":\"${var.nsx_cli_password}\"},{\"Key\":\"nsx_cli_audit_passwd_0\",\"Value\":\"${var.nsx_audit_password}\"},{\"Key\":\"nsx_hostname\",\"Value\":\"${"${var.nsx_edge_hostname}${count.index}"}\"},{\"Key\":\"nsx_gateway_0\",\"Value\":\"${var.nsx_gw}\"},{\"Key\":\"nsx_ip_0\",\"Value\":\"${lookup(var.nsx_edge_ips,count.index)}\"},{\"Key\":\"nsx_netmask_0\",\"Value\":\"${var.nsx_mask}\"},{\"Key\":\"nsx_dns1_0\",\"Value\":\"${var.nsx_dns}\"},{\"Key\":\"nsx_domain_0\",\"Value\":\"${var.nsx_domain}\"},{\"Key\":\"nsx_ntp_0\",\"Value\":\"${var.nsx_ntp}\"},{\"Key\":\"nsx_isSSHEnabled\",\"Value\":\"${var.nsx_ssh_enabled}\"},{\"Key\":\"nsx_allowSSHRootLogin\",\"Value\":\"${var.nsx_ssh_root}\"}],\"NetworkMapping\":[{\"Name\":\"Network 0\",\"Network\":\"${var.nsx_net_mgmt}\"},{\"Name\":\"Network 1\",\"Network\":\"${var.nsx_net_tep}\"},{\"Name\":\"Network 2\",\"Network\":\"${var.nsx_net_uplink1}\"},{\"Name\":\"Network 3\",\"Network\":\"${var.nsx_net_uplink2}\"}],\"MarkAsTemplate\":false,\"PowerOn\":false,\"InjectOvfEnv\":true,\"WaitForIP\":false,\"Name\":\"${"${var.nsx_edge_vmname}${count.index}"}\"}"
  filename = "nsx_edge_${count.index}_options.json"
}

# ---------------------------------------------------
# deploy the ova now

resource "null_resource" "nsx_mgr_deploy_govc" {
  depends_on = ["local_file.nsx_mgr_json"]
  triggers {
    key  = "${var.project_id}_nsx_mgr_deploy"
    name = "${var.nsx_mgr_vmname}"
  }
  provisioner "local-exec" {
    command = "govc import.ova --options=nsx_mgr_options.json ${var.nsx_mgr_ova}"
    environment {
      GOVC_INSECURE      = 1
      GOVC_URL           = "https://${var.root_vc_name}"
      GOVC_USERNAME      = "${var.root_vc_user}"
      GOVC_PASSWORD      = "${var.root_vc_pass}"
      GOVC_DATASTORE     = "${var.root_vc_ds}"
      GOVC_NETWORK       = "${var.nsx_net_mgmt}"
      GOVC_RESOURCE_POOL = "${null_resource.resource_pool.triggers.pool}"
	    GOVC_FOLDER        = "${vsphere_folder.vc_folder.path}"
    }
  }
}

resource "null_resource" "nsx_ctrl_deploy_govc" {
  depends_on = ["local_file.nsx_ctrl_json", "null_resource.nsx_mgr_deploy_govc"]
  count = "${length(var.nsx_ctrl_ips)}"
  triggers {
    key  = "${var.project_id}_nsx_ctrl_deploy"
    name = "${"${var.nsx_ctrl_vmname}${count.index}"}"
  }
  provisioner "local-exec" {
    command = "govc import.ova --options=${"nsx_ctrl_${count.index}_options.json"} ${var.nsx_ctrl_ova}"
    environment {
      GOVC_INSECURE      = 1
      GOVC_URL           = "https://${var.root_vc_name}"
      GOVC_USERNAME      = "${var.root_vc_user}"
      GOVC_PASSWORD      = "${var.root_vc_pass}"
      GOVC_DATASTORE     = "${var.root_vc_ds}"
      GOVC_NETWORK       = "${var.nsx_net_mgmt}"
      GOVC_RESOURCE_POOL = "${null_resource.resource_pool.triggers.pool}"
	    GOVC_FOLDER        = "${vsphere_folder.vc_folder.path}"
	  }
  }
}

resource "null_resource" "nsx_edge_deploy_govc" {
  depends_on = ["local_file.nsx_edge_json", "null_resource.nsx_ctrl_deploy_govc" ]
  count = "${length(var.nsx_edge_ips)}"
  triggers {
    key  = "${var.project_id}_nsx_edge_deploy"
    name = "${"${var.nsx_edge_vmname}${count.index}"}"
  }
  provisioner "local-exec" {
    command = "govc import.ova --options=${"nsx_edge_${count.index}_options.json"} ${var.nsx_edge_ova}"
    environment {
      GOVC_INSECURE = 1
      GOVC_URL = "https://${var.root_vc_name}"
      GOVC_USERNAME = "${var.root_vc_user}"
      GOVC_PASSWORD = "${var.root_vc_pass}"
      GOVC_DATASTORE = "${var.root_vc_ds}"
      GOVC_NETWORK = "${var.nsx_net_mgmt}"
      GOVC_RESOURCE_POOL = "${null_resource.resource_pool.triggers.pool}"
  	  GOVC_FOLDER = "${vsphere_folder.vc_folder.path}"
  	}
  }
}

# ---------------------------------------------------
# remove the reservations

resource "null_resource" "nsx_mgr_reconf" {
  depends_on = ["null_resource.nsx_mgr_deploy_govc"]
  triggers {
    key = "${var.project_id}_nsx_mgr_reconf"
  }
  provisioner "local-exec" {
    command = "govc vm.change -vm=${var.nsx_mgr_vmname} -cpu.reservation=0 -mem.reservation=0"
    environment {
      GOVC_INSECURE = 1
      GOVC_URL      = "https://${var.root_vc_name}"
      GOVC_USERNAME = "${var.root_vc_user}"
      GOVC_PASSWORD = "${var.root_vc_pass}"
  	}
  }
}

resource "null_resource" "nsx_ctrl_reconf" {
  depends_on = ["null_resource.nsx_ctrl_deploy_govc"]
  count = "${length(var.nsx_ctrl_ips)}"
  triggers {
    key = "${var.project_id}_nsx_ctrl_reconf"
  }
  provisioner "local-exec" {
    command = "govc vm.change -vm=${"${var.nsx_ctrl_vmname}${count.index}"} -cpu.reservation=0 -mem.reservation=0"
    environment {
      GOVC_INSECURE = 1
      GOVC_URL      = "https://${var.root_vc_name}"
      GOVC_USERNAME = "${var.root_vc_user}"
      GOVC_PASSWORD = "${var.root_vc_pass}"
    }
  }
}

resource "null_resource" "nsx_edge_reconf" {
  depends_on = ["null_resource.nsx_edge_deploy_govc"]
  count = "${length(var.nsx_edge_ips)}"
  triggers {
    key = "${var.project_id}_nsx_edge_reconf"
  }
  provisioner "local-exec" {
    command = "govc vm.change -vm=${"${var.nsx_edge_vmname}${count.index}"} -cpu.reservation=0"
    environment {
      GOVC_INSECURE = 1
      GOVC_URL = "https://${var.root_vc_name}"
      GOVC_USERNAME = "${var.root_vc_user}"
      GOVC_PASSWORD = "${var.root_vc_pass}"
	  }
  }
}

# ---------------------------------------------------
# power on

resource "null_resource" "nsx_poweron" {
  depends_on = ["null_resource.nsx_edge_reconf"]
  triggers {
    key = "${var.project_id}_nsx_poweron"
  }
  provisioner "local-exec" {
    command = "govc vm.power -k -M -on ${null_resource.nsx_mgr_deploy_govc.triggers.name} ${join(" ",null_resource.nsx_ctrl_deploy_govc.*.triggers.name)} ${join(" ",null_resource.nsx_edge_deploy_govc.*.triggers.name)}"
    environment {
      GOVC_INSECURE = 1
      GOVC_URL      = "https://${var.root_vc_name}"
      GOVC_USERNAME = "${var.root_vc_user}"
      GOVC_PASSWORD = "${var.root_vc_pass}"
	  }
  }
}
/*
GOVC_RESOURCE_POOL = "${null_resource.resource_pool.triggers.pool}"
${null_resource.nsx_mgr_deploy_govc.triggers.name}
${null_resource.nsx_ctrl_deploy_govc.*.triggers.name}
${null_resource.nsx_edge_deploy_govc.*.triggers.name}

join(",", aws_instance.foo.*.id)

govc vm.power -k -M -on test toto

"null_resource" "nsx_edge_deploy_govc" {
  depends_on = ["local_file.nsx_edge_json", "null_resource.nsx_ctrl_deploy_govc" ]
  count = "${length(var.nsx_edge_ips)}"
  triggers {
    key  = "${var.project_id}_nsx_edge_deploy"
    name = 

value = ["${aws_s3_bucket.s3_bucket.*.bucket}"]

Usage: govc vm.power [OPTIONS]

Options:
  -M=false               Use Datacenter.PowerOnMultiVM method instead of VirtualMachine.PowerOnVM
  -force=false           Force (ignore state error and hard shutdown/reboot if tools unavailable)
  -off=false             Power off
  -on=false              Power on
  -r=false               Reboot guest
  -reset=false           Power reset
  -s=false               Shutdown guest
  -suspend=false         Power suspend

resource "null_resource" "nsx_mgr_deploy_govc" {
resource "null_resource" "nsx_ctrl_deploy_govc" {
resource "null_resource" "nsx_edge_deploy_govc" {

*/
# ---------------------------------------------------
# consider the import, see https://www.terraform.io/docs/import/usage.html
#
#resource "vsphere_virtual_machine" "nsx_mgr_vm" {
#  name = "${var.nsx_mgr_vmname}"
#  datastore_id     = "${data.vsphere_datastore.ds.id}"
#  network_interface { 
#	network_id = "${data.vsphere_network.net_mgmt.id}"
#  }
#  disk {
#    label = "disk0"
#    size  = 1
#  }
#  resource_pool_id = "${data.vsphere_resource_pool.pool.id}"
#  folder = "${vsphere_folder.vc_folder.path}"
#}
#
#resource "vsphere_virtual_machine" "nsx_ctrl_vm" {
#  count = "${length(var.nsx_ctrl_ips)}"
#  name = "${"${var.nsx_ctrl_vmname}${count.index}"}"
#  datastore_id     = "${data.vsphere_datastore.ds.id}"
#  network_interface { 
#	network_id = "${data.vsphere_network.net_mgmt.id}"
#  }
#  disk {
#    label = "disk0"
#    size  = 1
#  }
#  resource_pool_id = "${data.vsphere_resource_pool.pool.id}"
#  folder = "${vsphere_folder.vc_folder.path}"
#}
#
#resource "vsphere_virtual_machine" "nsx_edge_vm" {
#  count = "${length(var.nsx_edge_ips)}"
#  name = "${"${var.nsx_edge_vmname}${count.index}"}"
#  datastore_id     = "${data.vsphere_datastore.ds.id}"
#  network_interface { 
#	network_id = "${data.vsphere_network.net_mgmt.id}"
#  }
#  network_interface {
#	network_id = "${data.vsphere_network.net_tep.id}"
#  }
#  network_interface {
#	network_id = "${data.vsphere_network.net_uplink1.id}"
#  }
#  network_interface {
#	network_id = "${data.vsphere_network.net_uplink2.id}"
#  }
#  disk {
#    label = "disk0"
#    size  = 1
#  }
#  resource_pool_id = "${data.vsphere_resource_pool.pool.id}"
#  folder = "${vsphere_folder.vc_folder.path}"
#}

