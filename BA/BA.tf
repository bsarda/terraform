# variables
variable "vcserver" { default = "mgt-vc" }
variable "vcuser" { default = "administrator@vsphere.local" }
variable "vcpassword" { default = "VMware1!" }
variable "datacenter" { default = "HomeLab" }
variable "cluster" { default = "Dell_DCS-6005" }
variable "datastore" { default = "VMFS_ssd-Sandisk-960G" }
variable "projectInitials" { default = "BA" }
variable "projectName" { default = "Single Cloud" }

# Configure the VMware vSphere Provider
provider "vsphere" {
  user                 = "${var.vcuser}"
  password             = "${var.vcpassword}"
  vsphere_server       = "${var.vcserver}"
  allow_unverified_ssl = true
}

# Create a folder
resource "vsphere_folder" "BAFolder" {
  datacenter = "${var.datacenter}"
  path       = "${var.projectInitials} - ${var.projectName}"
}

# Create a virtual machine within the folder
resource "vsphere_virtual_machine" "e1baesx01" {
  datacenter   = "${var.datacenter}"
  cluster      = "${var.cluster}"
  name         = "e1baesx01"
  folder       = "${vsphere_folder.BAFolder.path}"
  vcpu         = 2
  memory       = 8192
  domain       = "emea.corp.local"
  time_zone    = "Europe/Paris"
  dns_suffixes = ["emea.corp.local"]
  dns_servers  = ["172.20.1.32"]
  
  network_interface {
    label              = "SYSTEM-Passthrough-pnic0"
    ipv4_address       = "172.20.12.44"
    ipv4_prefix_length = "24"
    ipv4_gateway       = "172.20.12.1"
  }
  disk {
    datastore          = "${var.datastore}"
    template           = "Templates/t_ESXi-6.5a-mk1_DC1"
  }
}

