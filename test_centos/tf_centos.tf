# Configure the VMware vSphere Provider
provider "vsphere" {
  user           = "administrator@vsphere.local"
  password       = "VMware1!"
  vsphere_server = "vcsa00.emea.corp.local"
  allow_unverified_ssl = true
}

# Create a folder
resource "vsphere_folder" "frontend" {
  datacenter = "DC-1"
  path = "frontend"
}

# Create a file
resource "vsphere_file" "package" {
  datacenter = "DC-1"
  #datastore = "pod1/NFS-da1"
  datastore = "LOCAL/n-esx00_LOCAL"
  source_file = "K:\\com.vmware.pso-1.0.0-50.package"
  destination_file = "com.vmware.pso-1.0.0-50.package"
}

# Create a virtual machine within the folder
resource "vsphere_virtual_machine" "web" {
  #datacenter = "Fold%201/DC-1"
  datacenter = "DC-1"
  cluster = "ComputeCluster-1"
  name   = "terraform-web"
  folder = "${vsphere_folder.frontend.path}"
  vcpu   = 2
  memory = 4096

  network_interface {
    label = "dvPG-1001_Transport"
  }

  disk {
	datastore = "pod1/NFS-da1"
    template = "t_CentOS-6.5"
  }
}

resource "vsphere_virtual_machine" "web" {
  #datacenter = "Fold%201/DC-1"
  datacenter = "DC-1"
  cluster = "ComputeCluster-1"
  name   = "terraform-web"
  folder = "${vsphere_folder.frontend.path}"
  vcpu   = 1
  memory = 512

  network_interface {
    label = "dvPG-1001_Transport"
  }

  disk {
	datastore = "pod1/NFS-da1"
    template = "t_CentOS-6.5"
  }
}
